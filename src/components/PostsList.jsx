import React from "react";
import PostItem from "./PostItem";

const PostsList = ({ posts, title }) => {
  return (
    <div className="posts-list">
      <h2>{title}</h2>
      {posts.map((post) => (
        <PostItem post={post} key={post.id} />
      ))}
    </div>
  );
};

export default PostsList;
