import "./styles/main.scss";
import React, { useState } from "react";
import PostsList from "./components/PostsList";
// import Counter from "./components/Counter";
// import ClassCounter from "./components/ClassCounter";

function App() {
  // const [value, setValue] = useState("Text");
  const [posts] = useState([
    { id: 1, title: "Javascript", body: "Programming language" },
    { id: 2, title: "HTML", body: "Markup language" },
    { id: 3, title: "React", body: "Javascript framework" },
  ]);

  return (
    <div className="App">
      {/* <Counter />
      <ClassCounter /> */}

      {/* 2-way-binding */}
      {/* <h2>{value}</h2>
      <input
        type="text"
        value={value}
        onChange={(event) => setValue(event.target.value)}
      /> */}
      <PostsList posts={posts} title={"Список постов"} />
    </div>
  );
}

export default App;
